/**
 * 
 */
package http.request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;

/**
 * @author Christoph Englich
 *
 */
public class HTTPRequest {
	
	protected HashMap<String, String> headers = new HashMap<String, String>();
	private byte[] content = null;
	
	protected HTTPRequest() {
		
	}

	public HTTPRequest(String req) {
		String[] lines = req.split(System.getProperty("line.separator"));
		String[] header = lines[0].split(" ");
		headers.put("type", header[0]);
		headers.put("path", header[1]);
		headers.put("protocol", header[2]);
	}
	
	public HTTPRequest(HashMap<String, String> headers) {
		this(headers, null);
	}
	
	public HTTPRequest(HashMap<String, String> headers, byte[] content) {
		this.headers = (HashMap<String, String>) headers.clone();
		if(content != null)
			this.headers.put("Content-Length", content.length + "");
		this.content = content;
	}
	
	public HTTPRequest(InputStream in) throws IOException {
		InputStreamReader inr = new InputStreamReader(in);
		BufferedReader bin = new BufferedReader(inr);
		
		//Reading the first line.	
		String line = bin.readLine();
		String[] header = line.split(" ");
		headers.put("type", header[0]);
		if (header[1].equals("/"))
			header[1] = "/index.html";
		headers.put("path", header[1]);
		headers.put("protocol", header[2]);
		
		//Reading further lines, stopp at a blank line
		while(bin.ready()) {
			line = bin.readLine();
			if (!line.equals("")) {
				header = line.split(": ");
				headers.put(header[0], header [1]);
			} else {
				break;
			}
		}
		
		//now we are gonna read the content
		if (headers.containsKey("Content-Length")) {
			//nasty way through char array due to buffer, but working
			char[] cc = new char[Integer.parseInt(headers.get("Content-Length"))];
			bin.read(cc);
			String s = new String(cc);
			content = s.getBytes();
		}
	}

	public String getPath() {
		return headers.get("path");
	}

	public void setPath(String path) {
		setHeader("path", path);
	}

	public String getType() {
		return headers.get("type");
	}

	public void setType(String type) {
		setHeader("type", type);
	}

	public String getProtocol() {
		return headers.get("protocol");
	}

	public void setProtocol(String protocol) {
		setHeader("protocol", protocol);
	}
	
	public String getHeader(String header) {
		return headers.get(header);
	}
	
	public void setHeader(String header, String value) {
		headers.put(header, value);
	}
	
	public byte[] getContent() {
		return content;
	}
	
	protected void setContent(byte[] content) {
		this.headers.put("Content-Length", content.length + "");
		this.content = content;
	}
	
	public void sendRequest(OutputStream out) throws IOException {
		HashMap<String, String> sh = (HashMap<String, String>) headers.clone();
		String headers;
		String line = sh.get("type") + " " + sh.get("path") + " " + sh.get("protocol") + "\n";
		headers = line;
		sh.remove("type");
		sh.remove("path");
		sh.remove("protocol");
		Iterator<String> it = sh.keySet().iterator();
		while(it.hasNext()) {
			String key = it.next();
			String value = sh.get(key);
			line = key + ": " + value + "\n";
			headers += line;
		}
		headers += "\n";
		out.write(headers.getBytes());
		if (content != null) {
			out.write(content);
		}
		out.flush();
	}
}
