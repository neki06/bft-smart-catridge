/**
 * 
 */
package http.request;



/**
 * @author Christoph Englich
 *
 */
public class PUTRequest extends HTTPRequest {

	public PUTRequest(byte[] content) {
		headers.put("type", "PUT");
		headers.put("protocol", "HTTP/1.1");
		headers.put("path", "/");
		setContent(content);
	}
}
