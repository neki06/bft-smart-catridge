/**
 * 
 */
package http;


/**
 * @author Christoph Englich
 *
 */
public class HTTP {

	public static String ERROR404() {
		return "HTTP/1.1 404 Not Found\n";
	}
	
	public static String ERROR500() {
		return "HTTP/1.1 500 Internal Server Error\n";
	}
	
	public static String SUCCESS(String headers, String content) {
		return "HTTP/1.1 200 OK\n" + "Content-Length: " + content.length() + "\n" + headers + "\n";
	}
	
	public static String SUCCESS(String headers, byte[] content) {
		return "HTTP/1.1 200 OK\n" + "Content-Length: " + content.length + "\n" + headers + "\n";
	}
}
