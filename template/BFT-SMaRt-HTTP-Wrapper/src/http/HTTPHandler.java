/**
 * 
 */
package http;

import http.request.HTTPRequest;

/**
 * @author Christoph Englich
 *
 */
public class HTTPHandler {
	
	public static HTTPResponse getHTTPResponse(HTTPRequest req, String docRoot) {
		if (req.getType().equals("GET"))
			return new GETHandler(req, docRoot);
		else if(req.getType().equals("PUT"))
			return new PUTHandler(req, docRoot);
		else	
			return null;
	}
}
