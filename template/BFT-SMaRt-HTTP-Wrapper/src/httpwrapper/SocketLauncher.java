package httpwrapper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketLauncher extends Thread {

	private ServerSocket s;
	private Socket socket = null;
	private String host;
	private int port;
	private Forwarder forwarder;
	private Server server;
	
	public SocketLauncher(ServerSocket s, String host, int port, Forwarder forwarder) {
		this.s = s;
		this.host = host;
		this.port = port;
		this.forwarder = forwarder;
	}
	
	public void run() {
		while(socket == null) {
			try {
				socket = s.accept();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			server = new Server(socket, new Socket(host, port), forwarder);
			server.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public Server getServer() {
		return server;
	}
}
