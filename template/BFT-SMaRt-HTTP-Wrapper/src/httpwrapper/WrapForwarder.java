/**
 * 
 */
package httpwrapper;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * @author Christoph Englich
 *
 */
public class WrapForwarder implements Forwarder {

	private String host;
	private boolean incremented = false;;
	
	public WrapForwarder(String host) {
		this.host = host;
	}
	
	public WrapForwarder(String host, boolean incremented) {
		this.host = host;
		this.incremented = incremented;
	}
	
	public void forward(Socket in, Socket out) throws IOException {
		LinkedList<Byte> bytes = new LinkedList<Byte>();
		InputStream ins = in.getInputStream();
		
		//reading out the bytes
		int number = 0;
		byte[] buffer;
		number = ins.available();
		while(number > 0) {
			buffer = new byte[number];
			ins.read(buffer);
			for(byte b: buffer)
				bytes.add(b);
			number = ins.available();
		}
		byte[] content =  new byte[bytes.size()];
		Iterator<Byte> it = bytes.iterator();
		int i = 0;
		while(it.hasNext()) {
			content[i++] = it.next();
		}
		Wrapper.wrap(content, host, incremented).sendRequest(out.getOutputStream());
	}
}
