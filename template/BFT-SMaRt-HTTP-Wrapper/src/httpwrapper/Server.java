/**
 * 
 */
package httpwrapper;

import java.io.IOException;
import java.net.Socket;

/**
 * @author Christoph Englich
 *
 */
public class Server extends Thread {

	protected Socket in, out;
	protected Forwarder forwarder;
	protected boolean run = true;
	
	public Server(Socket in, Socket out, Forwarder forwarder) {
		this.in = in;
		this.out = out;
		this.forwarder = forwarder;
	}
	
	public void run() {
		int counter = 0;
		while(run) {
			try {
				if(in.getInputStream().available() > 0) {
					System.out.println("ForwardServer: Forwarding...");
					forwarder.forward(in, out);
				} else {
//					if (counter == 1000) {
//						System.out.println("ForwardServer: Nothing available, sleep...");
//						counter = 0;
//					} else {
//						counter++;
//					}
					Thread.sleep(20);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			in.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void shutdown() {
		run =  false;
	}
}
