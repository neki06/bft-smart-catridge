/**
 * 
 */
package httpwrapper;

import http.request.HTTPRequest;

import java.io.IOException;
import java.net.Socket;

/**
 * @author christoph
 *
 */
public class SortForwarder implements Forwarder {

	/* (non-Javadoc)
	 * @see httpwrapper.Forwarder#forward(java.net.Socket, java.net.Socket)
	 */
	@Override
	public void forward(Socket in, Socket out) throws IOException {
		out.getOutputStream().write(Wrapper.unwrap(in.getInputStream()));
	}
	
	public void forward(Socket in, Socket out, Socket out2) throws IOException {
		HTTPRequest r = Wrapper.unwrapToHTTPRequest(in.getInputStream());
		if(r.getHeader("Incremented").equals("1"))
			r.sendRequest(out2.getOutputStream());
		else
			r.sendRequest(out.getOutputStream());
	}

}
