package config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HostConfig {
	private String BFT_PATH = "../BFT-SMaRt/";
	private String CONFIG_PATH = BFT_PATH + "/config/";
	private String HOSTS_CONFIG = CONFIG_PATH + "hosts.config";
	private String HOST_NAME_CONFIG = CONFIG_PATH +"hostname.config";
	private Map<Integer,ArrayList<String>> configMap = new HashMap<Integer,ArrayList<String>>();
	private String localHostId;
	
	public HostConfig() {
		try {
			FileReader fr = new FileReader(HOSTS_CONFIG);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				if (!line.startsWith("#")){
					String[] configArray = line.split(" ");
					ArrayList<String> host = new ArrayList<>();
					host.add(configArray[2]);
					configMap.put(Integer.parseInt(configArray[0]), host);
				}
			}
			br.close();
			fr.close();
			FileReader fr2 = new FileReader(HOST_NAME_CONFIG);
			BufferedReader br2 = new BufferedReader(fr2);
			
			String line2;
			while((line2 = br2.readLine()) != null){
				if (!line2.startsWith("#")){
					if (line2.startsWith("localhostid")){
						String[] configArray = line2.split(" ");
						localHostId = configArray[1];
					} else {
						System.out.println(line2);
						String[] configArray = line2.split(" ");
						ArrayList<String> host = configMap.get(Integer.parseInt(configArray[0]));
						host.add(configArray[1]);
						configMap.put(Integer.parseInt(configArray[0]), host);
					}
				}
			}
			br2.close();
			fr2.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getHostPort(int id) {
		return Integer.parseInt(configMap.get(id).get(0));
	}
	
	public String getHostName(int id) {
		return (String)configMap.get(id).get(1);
	}
	
	public int getLocalId() {
		return Integer.parseInt(localHostId);
	}
	
//	public static void main(String[] args){
//		HostConfig hc = new HostConfig();
//		System.out.println(hc.getHostName("0") + ":" + hc.getHostPort("0"));
//		System.out.println("own id = " + hc.getLocalId());
//	}
}
