/**
 * 
 */
package test;

import java.io.IOException;
import java.net.Socket;

/**
 * @author Christoph Englich
 *
 */
public class SocketWriter extends Thread {

	private Socket s;
	private String t;
	
	public SocketWriter(Socket s, String t) {
		this.s = s;
		this.t = t;
	}
	
	public void run() {
		try {
			Thread.sleep(200);
			s.getOutputStream().write(t.getBytes());
			s.getOutputStream().flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}