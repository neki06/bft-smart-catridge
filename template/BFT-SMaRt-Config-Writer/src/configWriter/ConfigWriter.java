package configWriter;

import reader.HostConfigReader;
import writer.ForwarderConfigWriter;
import writer.HostConfigWriter;

public class ConfigWriter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int id = 0;
		try {
			id = Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out
					.println("First argument needs to be the Id of the replica! aborting...");
			System.exit(1);
		}
		HostConfigReader reader = new HostConfigReader("", "");

		if (reader.getNum() != args.length - 1) {
			System.out
					.println("Different count of urls and replicas in the ownHost.config! Using Urls of ownHosts.config");
		}

		HostConfigWriter hcWriter = new HostConfigWriter();
		int portOfID = hcWriter.writeConfig(reader, id);
		if (portOfID == 0) {
			System.out.println("Could not write host.config!");
		} else {
			System.out.println("Config written to config/host.config!");
		}

		ForwarderConfigWriter fcWriter = new ForwarderConfigWriter();
		if (!fcWriter.writeConfig(reader, id, args)) {
			System.out.println("Could not write hostname.config!");
		} else {
			System.out.println("Config written to config/hostname.config!");
		}

	}

}
