/**
 * 
 */
package reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.InetSocketAddress;
import java.util.Hashtable;
import java.util.StringTokenizer;

import util.Config;

/**
 * @author Frieder
 * 
 */
public class HostConfigReader {

	private Hashtable<Integer, Config> servers = new Hashtable<Integer, Config>();

	/** Creates a new instance of ServersConfig */
	public HostConfigReader(String configHome, String fileName) {
		loadConfig(configHome, fileName);
	}

	private void loadConfig(String configHome, String fileName) {
		try {
			String path = "";
			String sep = System.getProperty("file.separator");

			// putting path together, either the specified one or
			// "config/ownHosts.config"
			if (configHome.equals("")) {
				if (fileName.equals(""))
					path = "config" + sep + "ownHosts.config";
				else
					path = "config" + sep + fileName;
			} else {
				if (fileName.equals(""))
					path = configHome + sep + "ownHosts.config";
				else
					path = configHome + sep + fileName;
			}
			// read config file
			FileReader fr = new FileReader(path);
			BufferedReader rd = new BufferedReader(fr);
			String line = null;
			while ((line = rd.readLine()) != null) {
				if (!line.startsWith("#")) {
					StringTokenizer str = new StringTokenizer(line, " ");
					if (str.countTokens() > 2) {
						int id = Integer.valueOf(str.nextToken());
						String host = str.nextToken();
						int port = Integer.valueOf(str.nextToken());
						this.servers.put(id, new Config(id, host, port));
					}
				}
			}
			fr.close();
			rd.close();
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}

	public InetSocketAddress getAddress(int id) {
		Config c = this.servers.get(id);
		if (c != null) {
			return new InetSocketAddress(c.getHost(), c.getPort());
		}
		return null;
	}

	public int getPort(int id) {
		Config c = this.servers.get(id);
		if (c != null) {
			return c.getPort();
		}
		return (Integer) null;
	}

	public String getHost(int id) {
		Config c = this.servers.get(id);
		if (c != null) {
			return c.getHost();
		}
		return null;
	}

	public int getNum() {
		return servers.size();
	}

}
