/**
 * 
 */
package util;

/**
 * @author Frieder
 * 
 */
public class Config {
	private int ID;
	private String host;
	private int port;

	public Config(int ID, String host, int port) {
		this.ID = ID;
		this.host = host;
		this.port = port;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
}
